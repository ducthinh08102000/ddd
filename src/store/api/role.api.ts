import { apiService } from "./base";

export const roleApi = apiService.injectEndpoints({
  endpoints: (build) => ({
    getRoles: build.query<RsqlResponse<Role[]>, ParamRsql>({
      query: ({
        direction = "",
        page = 0,
        query = "",
        size = 10,
        sort = "",
      }) => ({
        url: "/v1.0/roles/crud/search",
        method: "POST",
        body: { page, query, size, ...(direction ? { direction } : {}) },
      }),
    }),
  }),
});

export const { useGetRolesQuery } = roleApi;
