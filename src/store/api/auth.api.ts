import { ACCESS_TOKEN_KEY } from "@app/config/const";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import StorageServices from "@services/local.storage";

const fetchBaseQueryBasic = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_API_BASE_URL,
});

export const authApi = createApi({
  baseQuery: fetchBaseQueryBasic,
  endpoints: (build) => ({
    login: build.mutation<AuthorizationData, string>({
      query: (token) => ({
        url: "v1.0/authorize/token",
        method: "GET",
        headers: { Authorization: "Basic " + token },
      }),
      transformResponse: (data: AuthorizationData) => {
        console.log(data);
        if (data.access_token) {
          StorageServices.setData(ACCESS_TOKEN_KEY, data.access_token);
          window.location.href = "/";
        }
        return data;
      },
    }),
    logout: build.mutation({
      query: () => ({ url: "v1.0/authorize/logout", method: "GET" }),
    }),
  }),
});

export const { useLoginMutation, useLogoutMutation } = authApi;
