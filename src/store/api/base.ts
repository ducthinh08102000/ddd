import { ACCESS_TOKEN_KEY } from "@app/config/const";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import type {
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError,
} from "@reduxjs/toolkit/query";
import StorageServices from "@services/local.storage";
import { Mutex } from "async-mutex";

const clearSession = () => {
  localStorage.clear();
  sessionStorage.clear();
};

export const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_API_BASE_URL,
  timeout: 15000,
  prepareHeaders: (headers) => {
    const accessToken = StorageServices.getData(ACCESS_TOKEN_KEY, null);
    if (!accessToken) {
      window.location.href = "/auth/sign-in";
      return;
    }
    headers.set("authorization", `Bearer ${accessToken}`);
    return headers;
  },
});

const mutex = new Mutex();
const baseQueryWithReauth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  // wait until the mutex is available without locking it
  await mutex.waitForUnlock();
  let result = await baseQuery(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    // checking whether the mutex is locked
    if (!mutex.isLocked()) {
      const release = await mutex.acquire();
      try {
        const refreshResult = await baseQuery(
          "/refreshToken",
          api,
          extraOptions
        );
        if (refreshResult.data) {
          // retry the initial query
          result = await baseQuery(args, api, extraOptions);
        } else {
          clearSession();
          window.location.replace("/auth/sign-in");
        }
      } finally {
        // release must be called once the mutex should be released again.
        release();
      }
    } else {
      // wait until the mutex is available without locking it
      await mutex.waitForUnlock();
      result = await baseQuery(args, api, extraOptions);
    }
  }
  return result;
};

export const apiService = createApi({
  baseQuery: baseQueryWithReauth,
  endpoints: () => ({}),
});
