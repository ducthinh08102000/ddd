import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
  initialState: "",
  reducers: {},
  name: "auth",
});

export default authSlice.reducer;
