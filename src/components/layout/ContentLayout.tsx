import { Spin } from "antd";
import React from "react";

const ContentLayout = ({ children, title, bgColor, loading }: any) => {
  return (
    <div
      style={bgColor ? { background: bgColor } : {}}
      className="content-layout"
    >
      <Spin spinning={!!loading} tip={"Đang xử lý"}>
        {children}
      </Spin>
    </div>
  );
};

export default React.memo(ContentLayout);
