import { sideNav } from "@app/config/sideNav";
import React from "react";
import { Link, useLocation } from "react-router-dom";

const SideBar = () => {
  const { pathname } = useLocation();
  console.log(pathname);
  return (
    <div className="side-nav">
      <div className="h-74 flex items-center justify-center">
        <span className="text-19 font-700 text-dark-blue leading-19">
          Quản trị hệ thống, báo cáo
        </span>
      </div>
      <div className="side-nav-menu pl-34">
        {sideNav.map((item) => {
          return (
            <div key={item.key} className="side-nav-group mb-28">
              <div className="side-nav-group-title text-15 font-600 h-23 mb-10">
                {item.group}
              </div>
              {item.menu.map((link) => (
                <Link
                  to={"/" + link.path}
                  key={link.key}
                  className={`menu-link ${
                    pathname.includes(link.path) && link.path
                      ? "active"
                      : link.key === "dashboard" && pathname === "/"
                      ? "active"
                      : ""
                  } pl-20 ls-75 text-15 h-23`}
                >
                  {link.title}
                </Link>
              ))}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(SideBar);
