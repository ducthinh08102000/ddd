import React from "react";
import { Outlet } from "react-router-dom";
import Header from "../Header";
import "./style.scss";

const AuthLayout = () => {
  return (
    <div>
      <div className="auth-header">
        <Header />
      </div>
      <div className="app-content-wrapper">
        <Outlet />
      </div>
      <div className="auth-footer">
        <div className="auth-container relative h-full">
          <div className="auth-footer-logo">
            <img src="/assets/image/logo-white.png" alt="logo-white" />
          </div>
          <div className="font-700 text-15">TỔ CHỨC GIÁO DỤC IIG VIỆT NAM</div>
          <div className="flex justify-between items-start mt-40">
            <div className="leading-20">
              <p className="text-11 font-400 mb-32">
                <span className="font-700 text-11">TRỤ SỞ CHÍNH</span>
                <br />
                75 Giang Văn Minh, Q. Ba Đình, Hà Nội
              </p>
              <p className="text-11 font-400">
                <span className="font-700 text-11">
                  CHI NHÁNH TP. HỒ CHÍ MINH
                </span>
                <br />
                Tầng 1, Tháp 1, The Sun Avenue, 28 Mai
                <br />
                Chí Thọ, An Phú, TP.Thủ Đức, TP.HCM
              </p>
            </div>
            <div className="leading-20">
              <p className="text-11 font-400 mb-18">
                <span className="font-700 text-11">
                  ĐỊA ĐIỂM ĐĂNG KÝ THI TẠI HÀ NỘI
                </span>
                <br />
                Tầng 3, Trung Yên Plaza, 1 Trung Hòa,
                <br />
                Q. Cầu Giấy, Hà Nội
              </p>
              <p className="text-11 font-400">
                <span className="font-700 text-11">CHI NHÁNH TP. ĐÀ NẴNG</span>
                <br />
                19 Hoàng Văn Thụ, Q. Hải Châu,
                <br />
                Đà Nẵng
              </p>
            </div>
            <div>
              <p className="my-0 font-700 text-11 leading-20">Liên hệ</p>
              <div className="text-11 font-400  leading-30">
                info@iigvietnam.edu.vn
              </div>
              <div className="text-11 font-400 leading-30">1900 636 929</div>
              <p className="my-0 font-700 text-11 leading-20">
                Social Network:
              </p>
              <div className="flex items-center">
                {["facebook", "youtube", "zalo"].map((x) => {
                  return (
                    <img
                      className="mr-21"
                      src={`/assets/svg/${x}-logo.svg`}
                      alt={x}
                    />
                  );
                })}
              </div>
            </div>
            <div className="text-11 leading-25">
              <div className="leading-20 font-700">Thời gian làm việc</div>
              <div>Sáng : 08:00 - 12:00</div>
              <div className="mb-16">{"Thứ Hai - Thứ Bảy"}</div>
              <div>Chiều: 13:30 - 17:30</div>
              <div>{"Thứ Hai - Thứ Sáu"}</div>
            </div>
          </div>
          <div className="end left-0 right-0 bottom-0 absolute h-55 text-15 flex justify-center items-center">
            © 2022. Toàn bộ bản quyền thuộc IIG VIỆT NAM
          </div>
          <img
            src="/assets/svg/triangle.svg"
            alt="triangle"
            className="absolute right--130 top--37"
          />
          <img
            src="/assets/svg/triangle.svg"
            alt="triangle"
            className="absolute bottom-0 left--141"
          />
        </div>
      </div>
    </div>
  );
};

export default React.memo(AuthLayout);
