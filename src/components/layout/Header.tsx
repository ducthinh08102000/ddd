import { ACCESS_TOKEN_KEY, AVATAR_DEFAULT } from "@app/config/const";
import StorageServices from "@services/local.storage";
import { useLogoutMutation } from "@store/api/auth.api";
import { Dropdown, MenuProps, notification } from "antd";
import React, { useMemo } from "react";
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { useAppSelector } from "@store/index";

const Header = () => {
  // const [logout] = useLogoutMutation();
  const user = useAppSelector((state) => state.user) as User;
  console.log(user);
  const dropMenu = useMemo<MenuProps["items"]>(() => {
    return [
      {
        key: "1",
        label: (
          <div className="flex items-center justify-between">
            <div className="flex items-center">
              <img
                className="w-29 h-29 rounded-50 object-cover mr-8"
                src={user?.avatar ?? AVATAR_DEFAULT}
                alt="avatar"
              />
              <p className="my-0 text-11 font-600 text-dark-blue">Học sinh</p>
            </div>

            <CaretUpOutlined className="text-dark-blue" />
          </div>
        ),
      },
      {
        key: "2",
        label: (
          <Link to="/learning-report" className="text-11 font-500">
            <span className="text-dark-blue">Theo dõi việc học của con</span>
          </Link>
        ),
      },
      {
        key: "3",
        label: (
          <Link to="/profile" className="text-11 font-500">
            <span className="text-dark-blue">Thông tin cá nhân</span>
          </Link>
        ),
      },
      {
        key: "4",
        label: (
          <div
            className="text-11 font-500"
            onClick={() => {
              localStorage.clear();
              window.location.reload();
            }}
          >
            <span className="text-dark-blue">Đăng xuất</span>
          </div>
        ),
      },
    ];
  }, [user?.avatar, user?.fullName]);

  return (
    <div className="app-header content-layout">
      <div className="flex justify-between w-full items-center h-full px-10">
        <img src="/assets/image/logo.png" alt="logo" />
        <div className="flex">
          <Link
            className="mr-38 text-15 font-600 text-dark-blue"
            style={{ textDecoration: "none" }}
            to="/"
          >
            Trang chủ
          </Link>
          <Link
            className="mr-38 text-15 font-600 text-dark-blue"
            style={{ textDecoration: "none" }}
            to="/courses"
          >
            Khóa học
          </Link>
          <Link
            className="mr-38 text-15 font-600 text-dark-blue"
            style={{ textDecoration: "none" }}
            to="/live-class"
          >
            Live class
          </Link>
          <Link
            className="text-15 font-600 text-dark-blue"
            style={{ textDecoration: "none" }}
            to="/exam-school"
          >
            Trường thi
          </Link>
        </div>
        {StorageServices.getData(ACCESS_TOKEN_KEY, null) ? (
          <Dropdown
            trigger={["click"]}
            overlayClassName={"custom-header-dropdown"}
            menu={{ items: dropMenu }}
          >
            <div className="flex items-center justify-between min-w-180 header-profile-button">
              <div className="flex items-center">
                <img
                  className="w-29 h-29 rounded-50 object-cover mr-8"
                  src={user?.avatar ?? AVATAR_DEFAULT}
                  alt="avatar"
                />
                <p className="my-0 text-11 font-600 text-dark-blue">
                  {user?.fullName ?? ""}
                </p>
              </div>

              <CaretDownOutlined className="text-dark-blue" />
            </div>
          </Dropdown>
        ) : (
          <div className="flex items-center">
            <Link
              className="mr-12 text-15 font-600 text-dark-blue"
              style={{ textDecoration: "none" }}
              to="/auth/sign-in"
            >
              Đăng nhập
            </Link>
            <Link
              className="text-15 font-600 text-dark-blue flex items-center justify-center rounded-50 h-40 w-100"
              style={{ textDecoration: "none", border: "1px solid #13345F" }}
              to=""
            >
              Đăng kí
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};

export default React.memo(Header);
