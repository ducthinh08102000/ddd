import { useMeMutation } from "@store/api/user.api";
import React, { useEffect } from "react";

const LoadData = () => {
  const [getMeData] = useMeMutation();
  useEffect(() => {
    getMeData(undefined);
  }, [getMeData]);
  return <></>;
};
export default React.memo(LoadData);
