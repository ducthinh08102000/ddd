import { Button, ButtonProps } from "antd";
import React from "react";
import "./style.scss";

interface PrimaryButtonProps extends ButtonProps {
  children: any;
}

const PrimaryButton = (props: PrimaryButtonProps) => {
  return (
    <Button {...props} className={`custom-button ${props.className}`}>
      {props.children}
    </Button>
  );
};

export default React.memo(PrimaryButton);
