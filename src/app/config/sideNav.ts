export const sideNav = [
  {
    key: 1,
    group: "Tổng quan",
    menu: [
      {
        path: "",
        key: "dashboard",
        title: "Dashboard",
      },
    ],
  },
  {
    key: 2,
    group: "Quản trị hệ thống",
    menu: [
      {
        path: "manage-system/user",
        key: "manage-system/user",
        title: "Quản lý người dùng hệ thống",
      },
      {
        path: "manage-system/student",
        key: "manage-system/student",
        title: "Quản lý học sinh IIG",
      },
      {
        path: "manage-system/school",
        key: "manage-system/school",
        title: "Quản lý trường",
      },
      {
        path: "manage-system/subdomain-school",
        key: "manage-system/subdomain-school",
        title: "Tạo Sub Domain cho trường",
      },
    ],
  },
  {
    key: 3,
    group: "Quản lý báo cáo tiến trình học",
    menu: [
      {
        path: "manage-learning-progress/student",
        key: "manage-learning-progress/student",
        title: "Danh sách học sinh",
      },
      {
        path: "manage-learning-progress/school",
        key: "manage-learning-progress/school",
        title: "Danh sách trường",
      },
      // {
      //   path: "manage-learning-progress/block",
      //   key: "manage-learning-progress/block",
      //   title: "Tiến trình học khối",
      // },
      // {
      //   path: "manage-learning-progress/class",
      //   key: "manage-learning-progress/class",
      //   title: "Tiến trình học lớp",
      // },
    ],
  },
  {
    key: 4,
    group: "Quản lý chương trình học",
    menu: [
      {
        path: "manage-study-program/course",
        key: "manage-study-program/course",
        title: "Danh sách khóa học",
      },
      {
        path: "manage-study-program/school-day",
        key: "manage-study-program/school-day",
        title: "Danh sách ngày học",
      },
      {
        path: "manage-study-program/learning",
        key: "manage-study-program/learning",
        title: "Theo dõi việc học",
      },
    ],
  },
  {
    key: 5,
    group: "Quản lý bài thi",
    menu: [
      {
        path: "manage-exam/list",
        key: "manage-exam/list",
        title: "Danh sách bài thi",
      },
      {
        path: "manage-exam/result",
        key: "manage-exam/result",
        title: "Kết quả thi",
      },
    ],
  },
  {
    key: 6,
    group: "Thư viện câu hỏi",
    menu: [
      {
        path: "manage-questions/list",
        key: "manage-questions/list",
        title: "Danh sách câu hỏi",
      },
    ],
  },
  {
    key: 7,
    group: "Quản lý lịch giảng dạy",
    menu: [
      {
        path: "manage-teaching-schedule/schedule",
        key: "manage-teaching-schedule/schedule",
        title: "Thời khóa biểu",
      },
      {
        path: "manage-teaching-schedule/statistical",
        key: "manage-teaching-schedule/statistical",
        title: "Thống kê giảng dạy",
      },
    ],
  },
];
