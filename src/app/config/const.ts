export const ACCESS_TOKEN_KEY = "accessToken";
export const REFRESH_TOKEN_KEY = "refreshToken";

export const AVATAR_DEFAULT = "/assets/image/avatar-default.jpg";
