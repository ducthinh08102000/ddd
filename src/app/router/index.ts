import { FunctionComponent, lazy, LazyExoticComponent } from "react";

const SignInView = lazy(() => import("@views/auth-views/sign-in"));
const HomePage = lazy(() => import("@views/app-views/home"));
const CoursesView = lazy(() => import("@views/app-views/courses"));
const LiveClassView = lazy(() => import("@views/app-views/live-class"));
const ProfilePageView = lazy(() => import("@views/app-views/profile"));
const LearningReportView = lazy(
  () => import("@views/app-views/learning-report")
);
const ExamSchoolView = lazy(() => import("@views/app-views/exam-school"));
const ExamView = lazy(() => import("@views/app-views/exam"));

interface Route {
  key: string;
  path: string;
  component: LazyExoticComponent<FunctionComponent | React.FC>;
  public: boolean;
  ready: boolean;
  nav: boolean;
}

export const routes: Route[] = [
  {
    key: "sign-in",
    path: "auth/sign-in",
    component: SignInView,
    public: true,
    ready: true,
    nav: true,
  },
  // Home page
  {
    key: "home",
    path: "",
    component: HomePage,
    public: true,
    ready: true,
    nav: true,
  },
  // Courses page
  {
    key: "courses",
    path: "/courses",
    component: CoursesView,
    public: false,
    ready: true,
    nav: true,
  },
  // Live class
  {
    key: "live-class",
    path: "/live-class",
    component: LiveClassView,
    public: false,
    ready: true,
    nav: true,
  },
  // Profile
  {
    key: "profile",
    path: "/profile",
    component: ProfilePageView,
    public: false,
    ready: true,
    nav: true,
  },
  // Learning report
  {
    key: "learning-report",
    path: "/learning-report",
    component: LearningReportView,
    public: false,
    ready: true,
    nav: true,
  },
  // Exam school
  {
    key: "exam-school",
    path: "/exam-school",
    component: ExamSchoolView,
    public: false,
    ready: true,
    nav: true,
  },
  {
    key: "exam",
    path: "/exam",
    component: ExamView,
    public: false,
    ready: true,
    nav: true,
  },
];

export const publicRoute = routes.filter((route) => route.public);
export const privateRoute = routes.filter((route) => !route.public);

export const sideNavigation = routes.filter((route) => route.nav);
console.log(
  sideNavigation.filter((x) => x.nav).map((x) => ({ path: x.path, key: x.key }))
);
