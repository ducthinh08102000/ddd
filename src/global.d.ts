// API
interface AuthorizationData {
  access_token: string;
  expires_in: number;
  message: "+OK";
  refresh_token: string | null;
  resetPassword: boolean;
  scope: "User";
  token_type: "JWT";
}

interface RsqlResponse<T> {
  data: T;
  page: {
    current_page: number;
    has_next: boolean;
    has_previous: boolean;
    total_elements: number;
    total_pages: number;
  };
  status: "SUCCESS" | "FAILED";
}

interface Response<T> {
  status: number;
  message: "SUCCESS" | "FAILED";
  description: string;
  apiStatus: "SUCCESS" | "FAILED";
  data: T;
}

interface MutationResponse<T> {
  status: number;
  message: "SUCCESS" | "FAILED";
  description: string;
  apiStatus: "SUCCESS" | "FAILED";
  data: T;
}

interface ParamRsql {
  query?: string;
  size?: number;
  page?: number;
  sort?: string;
  direction?: "ASC";
}

// Role
interface Role {
  id: number;
  name: string;
  createdTimestamp: string;
  lastUpdatedTimestamp: string;
  creationUserId: number;
  active: boolean;
  deleted: boolean;
  superAdmin: boolean;
}

// User
interface User {
  id: number;
  userName: string;
  password: string;
  fullName: string;
  email: string;
  mobile: string;
  address: string;
  age: number;
  sex: number;
  bod: string;
  jobTitle: string;
  createdTimestamp: string;
  lastUpdatedTimestamp: string;
  isDeleted: boolean;
  creationUserId: number;
  isSuspended: boolean;
  isApiDoc: any;
  avatar: string;
  isResetPassword: boolean;
}
