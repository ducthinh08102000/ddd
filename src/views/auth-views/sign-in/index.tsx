import { useLoginMutation } from "@store/api/auth.api";
import { Form, notification } from "antd";
import Button from "antd/es/button";
import FormItem from "antd/es/form/FormItem";
import Input from "antd/es/input";
import React, { useCallback, useEffect, useState } from "react";
import ForgotPasswordModal from "./components/ForgotPasswordModal";
import { LoadingOutlined } from "@ant-design/icons";
import { useForm } from "antd/es/form/Form";

const SignInView = () => {
  const [openForgotPass, setOpenForgotPass] = useState(false);
  const [token, setToken] = useState<string>("");
  const [form] = useForm();
  const [login, { isLoading }] = useLoginMutation();
  const toggleOpenForgotPass = useCallback((value: boolean) => {
    setOpenForgotPass(value);
  }, []);
  console.log(token);

  const onLogin = useCallback(
    async (values: { username: string; password: string }) => {
      try {
        login(btoa(`${values.username}:${values.password}`));
      } catch (err) {
        notification.error({ message: "Có lỗi khi đăng nhập" });
      }
    },
    [login]
  );
  return (
    <>
      <div className="content-layout">
        <div className="mt-138 mb-205 flex items-center">
          <div className="flex-1">
            <div className="text-19 font-700 leading-25 mb-33">
              Xin chào, <br />
              Đăng nhập và học nhiều điều bổ
              <br />
              ích từ IIG Việt Nam nhé
            </div>
            <Form
              form={form}
              initialValues={{ username: "admin", password: "iiG2o23aDm" }}
              onFinish={onLogin}
              layout="vertical"
            >
              <FormItem
                name="username"
                label={
                  <span className="text-15 leading-18 font-400">
                    Email đăng nhập
                  </span>
                }
              >
                <Input className="h-44 text-15" placeholder="Email..." />
              </FormItem>
              <FormItem
                name="password"
                label={
                  <span className="text-15 leading-18 font-400">Mật khẩu</span>
                }
              >
                <Input
                  className="h-44 text-15"
                  placeholder="Nhập mật khẩu..."
                />
              </FormItem>
            </Form>
            <div className="flex justify-end">
              <span
                onClick={() => toggleOpenForgotPass(true)}
                className="cursor-pointer text-dark-blue text-11 leading-16 translate-y--20"
              >
                Quên mật khẩu
              </span>
            </div>
            <Button
              onClick={() => form.submit()}
              type="primary"
              className="h-48"
              block
            >
              <span className="text-white font-700 text-19 flex items-center">
                {isLoading && <LoadingOutlined className="mr-8" />}
                Đăng nhập
              </span>
            </Button>
          </div>
          <img
            className="w-[536px] ml-90"
            src="/assets/image/sign-in-illustration.png"
            alt="sign-in-illustration"
          />
        </div>
        <ForgotPasswordModal
          open={openForgotPass}
          toggleOpen={toggleOpenForgotPass}
        />
      </div>
    </>
  );
};

export default SignInView;
