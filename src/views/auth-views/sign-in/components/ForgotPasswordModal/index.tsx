import React, { useState, useCallback } from "react";
import { Modal, Input, Form } from "antd";
import "./style.scss";
import Button from "antd/es/button";
import FormItem from "antd/es/form/FormItem";

type ModalType =
  | "input_email"
  | "msg_reset_success"
  | "input_new_pass"
  | "change_pass_success";

interface Props {
  open: boolean;
  toggleOpen: (value: boolean) => void;
}

const ForgotPasswordModal = ({ open, toggleOpen }: Props) => {
  const [type, setType] = useState<ModalType>("change_pass_success");
  return (
    <Modal
      centered
      className="forgot-pass-modal"
      style={{ borderRadius: 10, overflow: "hidden" }}
      closable={false}
      footer={false}
      open={open}
      onCancel={() => toggleOpen(false)}
    >
      <div className="flex justify-center items-center h-95 forgot-pass-header">
        <img src="/assets/image/logo-white.png" alt="logo white" />
      </div>
      <div className="pt-26 pb-53 px-66 min-h-372 flex flex-col justify-between">
        <Form layout="vertical">
          <div>
            <div className="text-center text-19 font-700 text-dark-blue leading-25 ls-75">
              {type === "input_email"
                ? "LẤY LẠI MẬT KHẨU"
                : type === "msg_reset_success"
                ? "Reset mật khẩu thành công!"
                : type === "change_pass_success"
                ? "Đổi mật khẩu thành công!"
                : "Nhập mật khẩu mới của bạn"}
            </div>
            {type === "input_email" && (
              <div className="mt-64">
                <FormItem
                  label={
                    <span className="text-15 leading-18 text-dark-blue">
                      Email
                    </span>
                  }
                >
                  <Input className="h-44 text-15" placeholder="Email..." />
                </FormItem>
              </div>
            )}
            {type === "msg_reset_success" && (
              <div className="mt-72 text-15 text-center">
                Chúng tôi đã gửi đường dẫn thiết lập mật khẩu mới vào email của
                bạn!
              </div>
            )}
            {type === "input_new_pass" && (
              <div className="mt-25">
                <FormItem
                  label={
                    <span className="text-15 text-dark-blue pl-10">
                      Nhập mật khẩu mới
                    </span>
                  }
                >
                  <Input className="h-44" placeholder="Nhập mật khẩu mới..." />
                </FormItem>
                <FormItem
                  label={
                    <span className="text-15 text-dark-blue pl-10">
                      Xác nhận lại mật khẩu
                    </span>
                  }
                >
                  <Input className="h-44" placeholder="Nhập lại mật khẩu..." />
                </FormItem>
              </div>
            )}
            {type === "change_pass_success" && (
              <div className="mt-72 text-15 text-center">
                Hãy đăng nhập bằng mật khẩu bạn vừa cập nhật!
              </div>
            )}
          </div>
        </Form>
        <div className="relative">
          <Button
            block
            className="h-48 text-19 font-700 leading-16 ls-25"
            type="primary"
          >
            <span>
              {type === "input_email"
                ? "Lấy lại mật khẩu"
                : type === "msg_reset_success"
                ? "Mở hòm thư"
                : type === "input_new_pass"
                ? "Lưu mật khẩu mới"
                : "Đăng nhập"}
            </span>
          </Button>
          {type === "input_email" && (
            <div className="text-15 absolute bottom--30 left-95 text-dark-blue">
              Đăng kí tài khoản mới
            </div>
          )}
        </div>
      </div>
    </Modal>
  );
};

export default React.memo(ForgotPasswordModal);
