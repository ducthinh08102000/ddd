import React from "react";
import { Link } from "react-router-dom";
import "./style.scss";

const ExamSchoolView = () => {
  return (
    <div className="content-layout pt-50 pb-100 exam-school">
      <div className="text-31 leading-46 font-700 text-center text-dark-blue mb-50">
        Trường thi
      </div>
      <div>
        {Array.from(new Array(3).keys()).map((x) => {
          return (
            <div
              className="exam-item flex justify-between items-center"
              key={x}
            >
              <div>
                <div className="text-19 font-600 text-dark-blue leading-22 mb-8">
                  Bài thi lớp 5:
                </div>
                <div className="text-light-blue text-15">
                  Các thí sinh làm bài thi trắc nghiệm trong 45 phút với 60 câu
                  hỏi
                </div>
                <div className="text-light-blue text-11 mt-20">
                  Kết thúc sau:
                  <span className="text-15 font-700">01:30:06</span>
                </div>
              </div>
              <Link to="/exam">
                <button className="text-15 font-700 rounded-50">
                  Làm bài thi
                </button>
              </Link>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(ExamSchoolView);
