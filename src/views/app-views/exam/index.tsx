import { Col, Radio, Row, Typography } from "antd";
import React from "react";
import "./style.scss";

const ExamView = () => {
  return (
    <div className="pt-62 pb-200 content-layout exam-page">
      <div className="py-20 px-36 question-list">
        <Row gutter={[0, 16]}>
          {Array.from(new Array(24).keys()).map((x) => {
            return (
              <Col span={2} key={x} className="flex justify-center">
                <div
                  className={`w-29 h-29 rounded-30 question-circle ${
                    x === 4 ? "active" : ""
                  }`}
                  style={{
                    background: [0, 1, 2, 3].includes(x)
                      ? "#0082FA"
                      : "#31527C",
                  }}
                ></div>
              </Col>
            );
          })}
        </Row>
      </div>
      <div className="mt-12 py-24 px-36 exam-card min-h-600">
        <div className="flex justify-between">
          <div className="flex items-center translate-x-8 justify-end h-32 w-83 relative exam-progress">
            <img
              src="/assets/svg/note-color-exam.svg"
              alt="note-icon"
              className="absolute left--8"
            />
            <div className="text-19 font-600 text-white translate-x--16">
              4/20
            </div>
          </div>
          <div className="exam-time relative items-center flex justify-end">
            <img
              src="/assets/svg/hourglass.svg"
              alt="note-icon"
              className="absolute left--16"
            />
            <div className="text-19 font-600 text-white translate-x--16">
              43:10
            </div>
          </div>
        </div>
        <div className="flex justify-center mb-16">
          <img
            src="https://imgur.com/114UCLb.png"
            className="h-202 object-contain"
            alt="img"
          />
        </div>
        <Typography.Paragraph
          style={{ color: "#D9DBE9" }}
          className="text-center text-19 font-500 text-white"
        >
          Ai là người đầu tiên trên thế giới phát hiện ra bóng đèn sợi đốt?
        </Typography.Paragraph>
        <div className="flex justify-center mb-38">
          <Radio.Group>
            <div className="max-w-786 m-auto custom-radio">
              <Row gutter={[24, 24]}>
                <Col span={12}>
                  <label
                    htmlFor="A"
                    className="p-24 exam-answer-button flex justify-between"
                  >
                    <span className="text-19 font-500 leading-24">
                      A. Voluptatum sodales
                    </span>
                    <Radio id="A" value={"A"} />
                  </label>
                </Col>
                <Col span={12}>
                  <label
                    htmlFor="B"
                    className="p-24 exam-answer-button flex justify-between"
                  >
                    <span className="text-19 font-500 leading-24">
                      B. Voluptatum sodales
                    </span>
                    <Radio id="B" value={"B"} />
                  </label>
                </Col>
                <Col span={12}>
                  <label
                    htmlFor="C"
                    className="p-24 exam-answer-button flex justify-between"
                  >
                    <span className="text-19 font-500 leading-24">
                      C. Voluptatum sodales
                    </span>
                    <Radio id="C" value={"C"} />
                  </label>
                </Col>
                <Col span={12}>
                  <label
                    htmlFor="D"
                    className="p-24 exam-answer-button flex justify-between"
                  >
                    <span className="text-19 font-500 leading-24">
                      D. Voluptatum sodales
                    </span>
                    <Radio id="D" value={"D"} />
                  </label>
                </Col>
              </Row>
            </div>
          </Radio.Group>
        </div>
        <div className="flex justify-between">
          <button className="exam-card-button">
            <img
              src="/assets/svg/arrow-right.svg"
              alt="img"
              style={{ transform: "rotate(180deg)" }}
            />
            <span className="ml-12">Trở lại</span>
          </button>
          <button className="exam-card-button">
            <span className="mr-12">Tiếp tục</span>
            <img src="/assets/svg/arrow-right.svg" alt="img" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(ExamView);
