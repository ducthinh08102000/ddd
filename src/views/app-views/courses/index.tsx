import { Col, Row } from "antd";
import React from "react";
import "./style.scss";

const CoursesView = () => {
  return (
    <div className="content-layout pt-45 pb-100 courses-page">
      <p className="leading-46 mb-32 text-center text-dark-blue font-700 text-31">
        Danh sách khóa học
      </p>
      <Row gutter={[24, 32]} align="stretch">
        {Array.from(new Array(6).keys()).map((x) => {
          return (
            <Col span={8} key={x}>
              <div className="px-12 pt-12 pb-30 courses-page-item rounded-20">
                <img
                  className="w-full h-166 object-cover"
                  src="https://imgur.com/9bdSKlO.png"
                  alt="course img"
                />
                <div className="leading-22 mt-16 font-600 text-19 text-dark-blue">
                  Tên khóa học
                </div>
                <p className="mt-10 mb-0 text-15 font-400 leading-22 text-light-blue">
                  Con được làm quen với các dạng bài tập giúp tăng khả năng quan
                  sát, ghi nhớ và kích thích tư duy tưởng tượng.
                </p>
                <div className="mt-16 flex courses-item-actions">
                  <button>Chương trình học</button>
                  <button>Tiếp tục học</button>
                </div>
              </div>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

export default React.memo(CoursesView);
