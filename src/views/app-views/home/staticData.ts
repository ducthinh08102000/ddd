export const features = [
  {
    id: 1,
    name: "Khóa học từ các\nchuyên gia",
    desc: "Khóa học là sự kết hợp giữ các chuyên gia nước ngoài về giáo dục sớm cho trẻ và các chuyên gia Việt Nam để tạo ra những khóa học phù hợp nhất với trẻ Việt Nam.",
    icon: "home-feature-icon-1.svg",
  },
  {
    id: 2,
    name: "Áp dụng AI vào đào\ntạo, phân phối bài học",
    desc: "Hệ thống AI sẽ thu thập các điểm mạnh và yếu của trẻ và sẽ phân phối tự động cho trẻ có thể phát triển được đều tất cả các kĩ năng.",
    icon: "home-feature-icon-2.svg",
  },
  {
    id: 3,
    name: "Video Live class update hàng tuần",
    desc: "Các video về các chủ đề như tiếng anh, kĩ năng sống, thể chất, toán học... Sẽ được các giao viên update hàng tuần.",
    icon: "home-feature-icon-3.svg",
  },
];

export const specialList = [
  {
    id: 1,
    name: "Chương trình giáo dục vượt trội",
    desc: "Tập trung vào 5 môn học chính: Kĩ năng sống thực tiễn, phát triển giác quan, logic toán học, ngôn ngữ và chữ viết, khoa học và văn hóa.",
    icon: "special-icon-1",
  },
  {
    id: 2,
    name: "Công nghệ AI được đưa vào khóa học",
    desc: "Hệ thống AI sẽ thu thập các điểm mạnh và yếu của trẻ và sẽ phân phối tự động cho trẻ có thể phát triển được đều tất cả các kĩ năng.",
    icon: "special-icon-2",
  },
  {
    id: 3,
    name: "Phát triển toàn diện cho trẻ",
    desc: "Con được làm quen với các dạng bài tập giúp tăng khả năng quan sát, ghi nhớ và kích thích tư duy tưởng tượng.",
    icon: "special-icon-3",
  },

  {
    id: 4,
    name: "Học như chơi thu hút được trẻ",
    desc: "Sử dụng các bài học bằng video hoạt hình, làm bài tập bằng các game không gây nhàm chán cho trẻ",
    icon: "special-icon-4",
  },
];

export const steps = [
  {
    id: 1,
    name: "Đăng kí học thử\nmiễn phí",
    desc: "Đăng kí tài khoản và cho con học thử và trải nghiệm miễn phí trên IIG.",
    icon: "step-icon-1",
  },
  {
    id: 2,
    name: "Lựa chọn khóa học\nđúng lứa tuổi",
    desc: "Lựa chọn khóa học đúng với lứa tuổi của con bạn để con có sự phát triển đúng với lộ trình.",
    icon: "step-icon-2",
  },
  {
    id: 3,
    name: "Kích hoạt tài khoản",
    desc: "Kích hoạt tài khoản để cho con có thể trải nghiệm tất cả các chức năng và công nghệ của IIG.",
    icon: "step-icon-3",
  },
  {
    id: 4,
    name: "Theo dõi sự phát\ntriển của con",
    desc: "Xem báo cáo thông kê về việc học, tiếp thu, phát triển của con bạn hàng tuần hoặc hàng tháng.",
    icon: "step-icon-4",
  },
];
