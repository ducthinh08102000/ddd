import { Carousel, Col, Rate, Row } from "antd";
import { CarouselRef } from "antd/es/carousel";
import { Ref, useRef } from "react";
import { features, specialList, steps } from "./staticData";
import "./style.scss";

const Home = () => {
  const carouselList1 = useRef<any>();
  const carouselList2 = useRef<any>();
  return (
    <div style={{ overflow: "hidden", maxWidth: "100vw" }}>
      <div className="content-layout">
        <div className="flex items-center">
          <div className="homepage-description">
            <div className="text-31 font-600 leading-46 mb-8">
              Nền tảng số 1 về đào tạo
            </div>
            <div className="text-31 font-600 leading-46 mb-12 text-dark-blue">
              Theo phương pháp
              <br /> Của trường Quốc Tế
            </div>
            <div className="font-500 text-19 text-dark-blue">
              Tăng cường phát triển toàn diện với các lĩnh vực chuyên biệt
            </div>
          </div>
          <img src="/assets/image/HomeBanner.png" alt="banner" />
        </div>
        <div className=""></div>
      </div>
      <div className="home-features">
        <div className="content-layout flex justify-between items-start">
          {features.map((x) => {
            return (
              <div className="flex items-start feature-item w-285">
                <div className="h-82 w-82 mr-8 rounded-20 flex">
                  <img
                    src={`/assets/svg/${x.icon}`}
                    alt="feature icon"
                    className="m-auto"
                  />
                </div>
                <div>
                  <p className="h-82 flex text-white text-15 font-500 items-center">
                    {x.name}
                  </p>
                  <div className="text-11 text-white">{x.desc}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <div className="mt-120 mb-105 bg-white">
        <div className="special content-layout flex items-end">
          <div className="special-image relative mr-24">
            <img
              className="absolute"
              src="/assets/image/boy.png"
              alt="boy img"
              width={313}
            />
          </div>
          <div className="special-content">
            <p className="leading-46 font-600 mb-30 text-31 special-title">
              {"Khóa học tại IIG có gì đặc\nbiệt"}
            </p>
            <div className="special-items">
              <Row gutter={[24, 12]}>
                {specialList.map((x) => {
                  return (
                    <Col span={12} key={x.id}>
                      <div className="flex items-start">
                        <div className="mr-8 w-42 h-42 flex shrink-0 rounded-10 special-icon">
                          <img
                            className="m-auto w-22  "
                            src={`/assets/svg/${x.icon}.svg`}
                            alt="special icon"
                          />
                        </div>
                        <div>
                          <div className="text-15 font-500 h-42 flex items-center">
                            {x.name}
                          </div>
                          <p className="mb-0 mt-12">{x.desc}</p>
                        </div>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </div>
          </div>
        </div>
      </div>
      <div className="content-layout steps">
        <Row align={"stretch"}>
          {steps.map((x) => {
            return (
              <Col span={6} key={x.id}>
                <div
                  className="h-full rounded-10 step-item pt-12 px-20 py-20"
                  style={{
                    background: [1, 3].includes(x.id)
                      ? "linear-gradient(286.33deg, #d7e8f2 8.45%, #f6fbff 93.14%)"
                      : "white",
                  }}
                >
                  <div className="flex step-icon">
                    <img
                      className="m-auto"
                      src={`/assets/svg/${x.icon}.svg`}
                      alt="step-icon"
                    />
                  </div>
                  <div className="flex items-center justify-center text-center text-15 font-700 step-name">
                    {x.name}
                  </div>
                  <p className="m-0 step-desc">{x.desc}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>

      <div className="flex justify-end pt-50 pb-40">
        <img
          src="/assets/svg/triangle.svg"
          alt="img"
          className="translate-x--24"
        />
      </div>
      <div className="content-layout courses hide-control-carousel relative">
        <p className="text-dark-blue text-31 font-700 leading-46 mt-0 mb-32">
          Danh sách khóa học
        </p>
      </div>
      <div
        style={{ maxWidth: 1000, margin: "0 auto" }}
        className="courses relative"
      >
        <div
          onClick={() => carouselList1.current && carouselList1.current.prev()}
          className="absolute course-button"
          style={{ top: "50%", left: -44 }}
        >
          <img
            src="/assets/svg/arrow-left-icon-white.svg"
            alt="arrow left img"
          />
        </div>
        <div
          onClick={() => carouselList1.current && carouselList1.current.next()}
          className="absolute course-button"
          style={{ top: "50%", right: -44, transform: "rotate(180deg)" }}
        >
          <img
            src="/assets/svg/arrow-left-icon-white.svg"
            alt="arrow left img"
          />
        </div>
        <Carousel ref={carouselList1}>
          {Array.from(new Array(3).keys()).map((y) => {
            return (
              <div key={y} style={{ padding: 20 }}>
                <Row align={"stretch"} gutter={24}>
                  {Array.from(new Array(3).keys()).map((x) => {
                    return (
                      <Col key={x} span={8}>
                        <div
                          className="flex flex-col rounded-20 p-12 items-center course"
                          style={{
                            boxShadow: [0, 1, 2].includes(x)
                              ? "0px 8px 20px 5px #e7ecf2"
                              : "",
                          }}
                        >
                          <img
                            src="/assets/image/avatar-default.jpg"
                            className="h-166 w-full course-img rounded-20"
                            alt="avatar"
                          />
                          <div className="course-name mt-16 text-19 text-dark-blue font-600">
                            Tên môn học
                          </div>
                          <p className="course-desc text-15 mt-10">
                            Con được làm quen với các dạng bài tập giúp tăng khả
                            năng quan sát, ghi nhớ và kích thích tư duy tưởng
                            tượng.
                          </p>
                          <div className="flex course-actions">
                            <button className="h-40 mr-16 flex items-center justify-center">
                              Tìm hiểu ngay
                            </button>
                            <button className="h-40 flex items-center justify-center">
                              Tìm hiểu ngay
                            </button>
                          </div>
                          <span className="block mt-18">
                            <Rate value={5} />
                            <span className="ml-8 text-14 font-600 text-dark-blue">
                              4.8 (20150)
                            </span>
                          </span>
                        </div>
                      </Col>
                    );
                  })}
                </Row>
              </div>
            );
          })}
        </Carousel>
      </div>
      <div className="flex justify-start pt-50 pb-40">
        <img src="/assets/svg/triangle.svg" alt="img" className="" />
      </div>
      <div className="content-layout feedback hide-control-carousel relative">
        <div
          onClick={() => carouselList2.current && carouselList2.current.prev()}
          className="absolute feedback-button"
          style={{ top: "50%", left: -64 }}
        >
          <img src="/assets/svg/arrow-left.svg" alt="arrow left img" />
        </div>
        <div
          onClick={() => carouselList2.current && carouselList2.current.next()}
          className="absolute feedback-button"
          style={{ top: "50%", right: -64, transform: "rotate(180deg)" }}
        >
          <img src="/assets/svg/arrow-left.svg" alt="arrow left img" />
        </div>
        <p className="text-dark-blue text-31 leading-46 mt-0 mb-0 text-center">
          Từ các phụ huynh đã cho con học IIG
        </p>
        <p
          className="text-19 font-500 text-center mb-28 mt-12"
          style={{ color: "#666666" }}
        >
          Hơn 500 ngàn bà mẹ tin tưởng và lựa chọn IIG
        </p>
        <Carousel ref={carouselList2} autoplay className="mb-205">
          {Array.from(new Array(3).keys()).map((y) => {
            return (
              <div key={y}>
                <Row align={"stretch"} gutter={60}>
                  {Array.from(new Array(3).keys()).map((x) => {
                    return (
                      <Col key={x} span={8}>
                        <div className="flex flex-col items-center feedback-item">
                          <img
                            src="/assets/image/avatar-default.jpg"
                            className="h-164 w-164 rounded-100"
                            alt="avatar"
                          />
                          <div className="mt-12 text-19 text-dark-blue font-700">
                            Nguyễn Hiền
                          </div>
                          <div
                            className="w-55 h-2 mt-24"
                            style={{ background: "#13345F" }}
                          />
                          <p className="text-15 text-center mt-21 feedback-content">
                            Trước giờ thấy học Tin học Văn phòng chán ngắt vì
                            toàn lý thuyết nhưng khóa online ở GoEdu làm mình
                            nghĩ khác hẳn. Nội dung học hỗ trợ người học từ A-Z,
                            có cả video hướng dẫn, cả bài thực hành xong cả thư
                            viện cho người học tham khảo. Nói chung mình thấy
                            xịn lắm nhé!
                          </p>
                        </div>
                      </Col>
                    );
                  })}
                </Row>
              </div>
            );
          })}
        </Carousel>
      </div>
    </div>
  );
};

export default Home;
