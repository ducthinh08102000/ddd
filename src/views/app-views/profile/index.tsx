import { AVATAR_DEFAULT } from "@app/config/const";
import {
  useMeMutation,
  useUpdateAvatarMutation,
  useUpdateUserMutation,
} from "@store/api/user.api";
import { useAppSelector } from "@store/index";
import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import FormItem from "antd/es/form/FormItem";
import { UploadChangeParam, UploadFile } from "antd/es/upload";
import Upload from "antd/es/upload/Upload";
import dayjs from "dayjs";
import React, { useCallback, useEffect, useState } from "react";

const Label = ({ children }: any) => (
  <span className="text-15 leading-20 text-dark-blue pl-11">{children}</span>
);

const ProfilePageView = () => {
  const [form] = Form.useForm();
  const user = useAppSelector((state) => state.user) as User;
  const [updateUserInfo] = useUpdateUserMutation();
  const [refetchMeData] = useMeMutation();
  const [loading, setLoading] = useState(false);
  const [updateMyAvatar] = useUpdateAvatarMutation();
  useEffect(() => {
    if (!user) return;
    const { fullName, address, bod, sex, mobile } = user as User;
    form.setFieldsValue({
      fullName,
      address,
      bod: dayjs(bod),
      sex,
      mobile,
    });
  }, [form, user]);

  const updateUser = useCallback(
    async ({ fullName, sex, bod, address, mobile }: any) => {
      try {
        const body = {
          fullName,
          sex,
          bod: dayjs(bod).format("YYYY-MM-DD HH:mm:ss"),
          address,
          mobileNumber: mobile,
        };
        const res = await updateUserInfo(body).unwrap();
        console.log(res);
      } catch (err) {}
    },
    [updateUserInfo]
  );

  const updateAvatar = useCallback(
    async (file: UploadChangeParam<UploadFile<any>>) => {
      try {
        if (loading) return;
        setLoading(true);
        const formData = new FormData();
        formData.append("fileToUpload", file.file.originFileObj as any);
        await updateMyAvatar(formData).unwrap();
        refetchMeData(undefined);
      } catch (err) {}
      setLoading(false);
    },
    [loading, refetchMeData, updateMyAvatar]
  );

  return (
    <div className="content-layout pt-50">
      <div className="flex items-center mb-50">
        <img
          src={user?.avatar ?? AVATAR_DEFAULT}
          alt="avatar"
          className="w-58 h-58 rounded-100 mr-24 object-cover"
        />
        <div className="text-15 font-600">
          {user?.fullName ?? ""} / Thông tin tài khoản
        </div>
      </div>
      <div className="max-w-690 m-auto">
        <div className="flex items-center mb-32">
          <img
            src={user?.avatar ?? AVATAR_DEFAULT}
            alt="avatar"
            className="w-94 h-94 rounded-100 mr-24 object-cover"
          />
          <Upload showUploadList={false} onChange={updateAvatar}>
            <Button type="primary" className="mx-16">
              Tải ảnh đại diện mới
            </Button>
          </Upload>
          <Button type="primary">Đổi mật khẩu</Button>
        </div>
        <Form layout="vertical" form={form} onFinish={updateUser}>
          <Row gutter={[80, 0]}>
            <Col span={12}>
              <FormItem
                name="fullName"
                label={<Label>Họ và tên học sinh</Label>}
              >
                <Input placeholder="Nhập tên học sinh" />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label={<Label>Họ và tên phụ huynh</Label>}>
                <Input placeholder="Nhập tên phụ huynh" />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem name="bod" label={<Label>Ngày sinh</Label>}>
                <DatePicker className="w-full" placeholder="Chọn ngày sinh" />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem name="address" label={<Label>Địa chỉ</Label>}>
                <Input placeholder="Nhập địa chỉ" />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem name="sex" label={<Label>Giới tinh</Label>}>
                <Select placeholder="Chọn giới tính">
                  <Select.Option value={0}>Nam</Select.Option>
                  <Select.Option value={1}>Nữ</Select.Option>
                  <Select.Option value={2}>Khác</Select.Option>
                </Select>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem name="mobile" label={<Label>Số điện thoại</Label>}>
                <Input placeholder="Nhập số điện thoại" />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem className="class" label={<Label>Lớp</Label>}>
                <Input disabled placeholder="Nhập lớp" />
              </FormItem>
            </Col>
          </Row>
          <div className="mt-30 mb-100">
            <Button type="primary" className="w-175" htmlType="submit">
              Cập nhật thông tin
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default React.memo(ProfilePageView);
