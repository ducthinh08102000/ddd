import React from "react";
import "./style.scss";

const LiveClassListView = () => {
  return (
    <div className="content-layout pt-45 pb-100 live-class">
      <p className="leading-46 mb-50 text-center text-dark-blue font-700 text-31">
        Live class
      </p>
      <div>
        {Array.from(new Array(3).keys()).map((x) => {
          return (
            <div
              className="live-class-item mb-32 flex justify-between items-center"
              key={x}
            >
              <div>
                <p className="my-0 text-dark-blue text-19 font-600 leading-22 mt-8">
                  Live class: Tin học bài 1
                </p>
                <p className="text-light-blue text-15 font-500 mt-8">
                  Ghi chú bài
                </p>
                <p className="text-light-blue font-700 text-15 mb-0">
                  <span className="text-11 font-500">Diễn ra vào:</span> 09:30
                  12/3/2023
                </p>
              </div>
              <button className="">Làm bài thi</button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(LiveClassListView);
