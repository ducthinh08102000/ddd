import { Col, Row, Select } from "antd";
import React, { useMemo } from "react";
import "./style.scss";
import { useAppSelector } from "@store/index";

const LearningReportView = () => {
  const user = useAppSelector((state) => state.user) as User;
  const learningReport = useMemo(
    () => [
      {
        id: 1,
        icon: "medal",
        label: "Số bài hoàn thành",
        value: "51",
      },
      {
        id: 2,
        icon: "wifi-speed-icon",
        label: "Số bài hoàn thành",
        value: "51",
      },
      {
        id: 3,
        icon: "chart-icon",
        label: "Số bài hoàn thành",
        value: "51",
      },
      {
        id: 4,
        icon: "analytics-icon",
        label: "Số bài hoàn thành",
        value: "51",
      },
    ],
    []
  );

  const examReportData = useMemo(
    () => [
      {
        id: 1,
        icon: "medal",
        label: "Số bài thi đã làm",
        value: "51",
      },
      {
        id: 2,
        icon: "wifi-speed-icon",
        label: "Tổng số điểm",
        value: "51",
      },
      {
        id: 3,
        icon: "chart-icon",
        label: "Điểm trung bình",
        value: "51",
      },
      {
        id: 4,
        icon: "note-icon",
        label: "Số bài thi không làm",
        value: "0",
      },
    ],
    []
  );

  return (
    <div className="content-layout pt-50 pb-100 learning-report">
      <div className="flex items-center">
        <img
          src="/assets/image/avatar-default.jpg"
          alt="avatar"
          className="w-58 h-58 rounded-100 mr-24 object-cover"
        />
        <div className="text-15 font-600">
          {user?.fullName ?? ""} / Thông tin tài khoản
        </div>
      </div>
      <div className="max-w-680 m-auto">
        <div className="flex justify-end mb-16">
          <Select defaultValue={"month"}>
            <Select.Option value="month">Tháng này</Select.Option>
          </Select>
        </div>
        <div className="report-card rounded-10 p-16 relative">
          <div
            className="w-245 absolute"
            style={{
              background: "#C2C2C2",
              height: 0.2,
              top: "54%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div
            className="h-245 absolute"
            style={{
              background: "#C2C2C2",
              width: 0.2,
              top: "54%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div className="text-center mb-12 text-15 font-600 text-white">
            Báo cáo việc học
          </div>
          <Row>
            {learningReport.map((x) => {
              return (
                <Col span={12} key={x.id}>
                  <div className="flex items-center h-134">
                    <div className="flex flex-col items-center flex-1">
                      <img
                        src={`/assets/svg/${x.icon}.svg`}
                        alt="report icon"
                      />
                      <span className="text-15 leading-21 mt-4 font-700 text-white">
                        {x.label}
                      </span>
                    </div>
                    <div className="flex flex-1">
                      <span
                        className="m-auto text-31 font-700"
                        style={{ color: "#00B4DA" }}
                      >
                        {x.value}
                      </span>
                    </div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
        <div className="report-card rounded-10 p-16 relative mt-24">
          <div
            className="w-245 absolute"
            style={{
              background: "#C2C2C2",
              height: 0.2,
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div
            className="h-245 absolute"
            style={{
              background: "#C2C2C2",
              width: 0.2,
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div className="text-center mb-12 text-15 font-600 text-white">
            Báo cáo bài thi
          </div>
          <Row>
            {examReportData.map((x) => {
              return (
                <Col span={12} key={x.id}>
                  <div className="flex items-center h-134">
                    <div className="flex flex-col items-center flex-1">
                      <img
                        src={`/assets/svg/${x.icon}.svg`}
                        alt="report icon"
                      />
                      <span className="text-15 leading-21 mt-4 font-700 text-white">
                        {x.label}
                      </span>
                    </div>
                    <div className="flex flex-1">
                      <span
                        className="m-auto text-31 font-700"
                        style={{ color: "#00B4DA" }}
                      >
                        {x.value}
                      </span>
                    </div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
      </div>
    </div>
  );
};

export default React.memo(LearningReportView);
