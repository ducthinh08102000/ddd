import { routes } from "@app/router";
import PrivateRoute from "@app/router/PrivateRoute";
import PublicRoute from "@app/router/PublicRoute";
import DevelopingView from "@components/common/DevelopingView";
import AppLayout from "@components/layout/AppLayout";
import NotFound from "@views/not-found";
import { Route, Routes } from "react-router-dom";
import "antd/dist/reset.css";
import "@assets/styles/main.scss";

function App() {
  return (
    <Routes>
      <Route path="/" element={<AppLayout />}>
        {routes.map((route) => {
          return (
            <Route
              key={route.key}
              path={route.path}
              element={
                route.ready ? (
                  route.public ? (
                    <PublicRoute>
                      <route.component />
                    </PublicRoute>
                  ) : (
                    <PrivateRoute>
                      <route.component />
                    </PrivateRoute>
                  )
                ) : (
                  <DevelopingView />
                )
              }
            />
          );
        })}
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
