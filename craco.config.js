const path = require("path");
module.exports = {
  webpack: {
    alias: {
      "@components": path.resolve(__dirname, "src", "components"),
      "@locale": path.resolve(__dirname, "src", "locale"),
      "@store": path.resolve(__dirname, "src", "store"),
      "@services": path.resolve(__dirname, "src", "services"),
      "@views": path.resolve(__dirname, "src", "views"),
      "@provider": path.resolve(__dirname, "src", "provider"),
      "@app": path.resolve(__dirname, "src", "app"),
      "@assets": path.resolve(__dirname, "src", "assets"),
    },
  },
};
